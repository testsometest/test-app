import React, { Component } from 'react';
import Bin from './Bin';
import Bomb from './Bomb';

const colors = ['lightblue', 'yellow', 'pink']

const bombs = [1, 2, 3, 4, 5]

const bombColors = ['lightblue', 'pink', 'green', 'yellow', 'blue']

const place = [{ left: '21px', top: '15px'}, { left: '221px',  top: '24px'}, { left: '420px', top: '72px'}, {left: '142px', top: '115px'}, {left: '383px', top: '147px'}]

class ChooseRandomColors extends Component {
	state = {
		colorsForBins: colors, 
		bombProperties: null,
		binProperties: null,
		x: false, 
		bombColors: bombColors,
		points: 0
		//bomb: null
	}

	componentDidMount() {
		//const initColors = this.state.colorsForBins;
		//console.log(initColors)
		setInterval(() => this.setColors(this.state.colorsForBins), 5000)
		//console.log('colors for bins', this.state.colorsForBins)
	}

	//poinst = () => {
	//	if (this.state.points =< 0) {

	//	}
	//}

	//getProperties = ({ name, value, id }) => {
	//	console.log('name', name)
	//	console.log('value', value)
	//	if (name === 'bombProperties') {
	//		this.setState({ bombProperties: value })
	//	}
	//	if (name === 'binProperties') {
	//		this.setState({ binProperties: value })
	//	}
	//	///const key = properties.name
		//const value = properties.value
		//name === 'binProperties' ? console.log('chooseRandomColors properties', name) : null;
		//name === 'bombProperties' ? console.log('chooseRandomColors properties', name) : null; //console.log(properties)		//this.setState({ [key]: value })
	//}

	getBomb = (bomb, color) => {
		//console.log('bomb from chooserandomcolors', bomb)
		this.setState({ bombProperties: {bomb: bomb, color: color }})
	}

	setColors = (colors) => {
		console.log(colors)
		const swapColors = (colors) => colors.filter(color => colors.indexOf(color) !== 0)
		const newColors = [ ...swapColors(colors), colors[0]]
		//const newColors = [ colors[2], colors[0], colors[1]
		//const [x, y, z] = colors
		//const newColors = [y, z, x]
		//console.log('color', x)
		//const x = swapColors(colors)
		//console.log('newColors', newColors)
		this.setState({ colorsForBins: newColors })
	} 

	removeBomb = color => {
		const newBombs = bombColors.filter(x=> x !== color);
		this.setState({ bombColors: newBombs })
	}

	makePointUp = () => this.setState((prevState, props) => ({ points: this.state.points + 1}))

	makePointDown = () => {
		if(this.state.points <= 0) {
			return null
		}// else {
			this.setState((prevState, props) => ({ points: this.state.points - 1 }))
		//}
	}

	//hideBomb = (bomb, bin)=> {
	//	console.log('bomb', bomb)
	//	console.log('bin', bin)
	//	console.log('bomb.left', bomb.left)
	//	console.log('bin.width', bin.with)
	//	if(bomb.left < bin.left + bin.width && 
    //  		bomb.left + bomb.width  > bin.left &&
    //  		bomb.top < bin.top + bin.height && 
    //  		bomb.top + bin.height > bin.top) 
    //  		{
    //		this.setState({ x: true })
	//	} else {
	//		this.setState({ x: false })
	//	}
	//}

	//componentDidUpdate(prevProps, prevState) {
	//	if(this.state.bombProperties !== prevState.bombProperties && this.state.binProperties) {
	//		this.hideBomb(this.state.bombProperties, this.state.binProperties)
	//	}
	//}

	//shouldComponentUpdate() {
//
	//}

	render() {
		//console.log('show or hide bomb', this.state.x)
		//console.log(this.state.bomb)
		//console.log('inBin', this.state.x)
		//console.log('bin state', this.state.binProperties)
		//console.log('bomb state', this.state.bombProperties)
		const { colorsForBins } = this.state;
		//console.log(this.state.bomb)
		return colorsForBins !== [] ? (
			<div>
				{this.state.bombColors.map((x, i) => <Bomb key={x} removeBomb={this.removeBomb} getBomb={this.getBomb} background={x} place={place[i]}/>)}
				<div>
					<div>Points {this.state.points}</div>
					{/*{colorsForBins.map(color => <Bin color={color} key={color}/>)}*/}
					<Bin makePointUp={this.makePointUp} makePointDown={this.makePointDown} removeBomb={this.removeBomb} bombProperties={this.state.bombProperties} color={colorsForBins[0]} binId='bin1'/>
					<Bin makePointUp={this.makePointUp} makePointDown={this.makePointDown} removeBomb={this.removeBomb} bombProperties={this.state.bombProperties} color={colorsForBins[1]} binId='bin2'/>
					<Bin makePointUp={this.makePointUp} makePointDown={this.makePointDown} removeBomb={this.removeBomb} bombProperties={this.state.bombProperties} color={colorsForBins[2]} binId='bin3'/>
				</div>
			</div>
		) : null;
	}
}

export default ChooseRandomColors;