import React, { Component } from 'react';

const style = {
	width: '35px',
	height: '35px',
	borderRadius: '50%',
	position: 'absolute',
    userSelect: 'none'            
}

class Bomb extends Component {
	state = {
		dragging: false, 
		x: this.props.place.left,
		y: this.props.place.top,
		bombProperties: null,
		isShowing: 5
	}

	componentDidMount() {
		document.addEventListener('mousemove', e => {
			//console.log(e)
			if (this.state.dragging && this.bomb) {
				this.setState({ 
					x: e.clientX,
					y: e.clientY,
					//bombProperties: this.bomb.getBoundingClientRect()
				})
				//this.setState({ bombProperties: })
				//this.props.changeColor(this.bomb)
				const bombProperties = this.bomb.getBoundingClientRect()
				//this.props.getProperties({name: 'bombProperties', value: bombProperties})
				//this.setState({ bombProperties })
				this.props.getBomb(bombProperties, this.bomb.style.background)
			}
		})
		this.removing = setTimeout(() => this.props.removeBomb(this.props.background), 5000)
		this.show = setInterval(() => this.setState({ isShowing: this.state.isShowing - 1}), 1000)
	}

	//changeColor = rect => {
	//	)
	//}

	componentWillUnmount() {
		clearTimeout(this.removing)
		clearInterval(this.show)
	}

	handlerMouseDown = e => {
		this.setState({ dragging: true })
	}

	// handleMouseMove = e => {

	// 	// e.preventDefault();
	// 	if (this.state.dragging) {
	// 		this.setState({ 
	// 			x: e.clientX,
	// 			y: e.clientY
	// 		})
	// 	}
	// }

	handleMouseUp = e => {
		// e.preventDefault();
		this.setState({ dragging: false })
	}

	//componentDidUpdate(prevProps, prevState) {
	//	if(prevState.dragging !== this.state.dragging) {
	//		this.setState({ color: true })
	//	}
	//}

	render() {
		
		return (
			<div 
				ref = { ref => this.bomb = ref }
				onMouseDown={this.handlerMouseDown} 
				// onMouseMove={this.handleMouseMove}
				onMouseUp={this.handleMouseUp}
				style={{...style, background: this.props.background, left: this.state.x, top: this.state.y }}
			>
				
				<span style={{color: '#fff'}}> {this.state.isShowing}</span>
			</div>
		)
	}
}

export default Bomb;