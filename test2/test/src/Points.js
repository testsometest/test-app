import React, ( Component } from 'react';

class Points extends Component {
	state = {
		points: 0
	}

	makePointUp = () => this.setState((prevState, props) => ({ points: this.state.points + 1}))

	makePointDown = () => this.setState((prevState, props) => ({ points: this.state.points - 1}))

	render() {
		render <div>{this.props.children(this.state.points)}</div>
	}
}

export default Points;