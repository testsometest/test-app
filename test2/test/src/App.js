import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import ChooseRandomColors from './ChooseRandomColors';
//import Bomb from './Bomb';

const style = {
	position: 'absolute',
	bottom: '0'
}

class App extends Component {
	state = {
		game: 120
	}

	componentDidMount() {
		setInterval(() => this.game(), 1000)
	}

	game = () => {
		if(this.state.game > 120) {
			this.setState({ game: 120 })
			console.log('new game')		
		} else {
			this.setState({ game: this.state.game - 1 })
		}
	}

    render() {
  	  return (
     	 <div>
        	{/*<Bomb/>*/}
        	<ChooseRandomColors/>
        	<div style={style}>{this.state.game}</div>
      	</div>
)
  }
}

export default App;
