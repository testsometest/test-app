import React, { Component } from 'react';

const bin = {
	width: '150px',
	height: '150px',
	float: 'left',
	marginLeft: '15px',
	marginRight: '15px',
	border: '1px solid lightblue',
	bin: false
}

class Bin extends Component {
	state = {
		hide: false
	}
	// give these picked point and current rect
	// when component will recieve props
	//componentDidMount() {
	//	if(this.props.bomb) {
	//		//this.hideBomb()
	//		//console.log('nextProps', nextProps)
	//		this.hideBomb(this.props.bomb)
	//	}
		//const binProperties = this.bin.getBoundingClientRect()
		//console.log('bin')
		//const self = this;
		//const properties = {
		//	name: 'binProperties',
		//	value: self.bin.getBoundingClientRect()
		//}

		//console.log('bin value', properties)
		//const binProperties = this.bin.getBoundingClientRect()
		//this.props.getProperties({ name: 'binProperties', value: binProperties, id: this.props.binId })
	//}

	componentWillReceiveProps(nextProps) {
		if(this.props.bombProperties && this.props.bombProperties !== nextProps.bombProperties) {
			//this.hideBomb()
			//console.log('this props bomb', this.props.bomb)
			this.hideBomb(this.props.bombProperties)
		}
	}

	hideBomb = bombProperties => {
		let bin = this.bin.getBoundingClientRect()
		let { bomb, color } = this.props.bombProperties
		//console.log('hide bomb bin', bin)
		//console.log('bomb', bomb, bin)
		//console.log('bomb left', bomb.left)
		//console.log('bomb top', bomb.top)

		if (bomb.left > bin.left && 
			bomb.left < bin.left + bin.width && 
			bomb.top > bin.top && 
			bomb.top < bin.height + bin.top) {
			//console.log('bomb left', bomb.left)
			this.point(color)
			//this.setState({ hide: true })
		} else {
			//this.setState({ hide: false })
		}
		//this.setState({ hide: true })
	}

	showBomb = () => {
		this.setState({ hide: false })
	}

	//changeColor = () => {
	//	const binProperties = this.bin.getBoundingClientRect()
	//	this.props.hideBomb(null, binProperties)

	point = bombColor => {
		console.log(bombColor === this.bin.style.background)
		if (bombColor === this.bin.style.background) {
			this.props.removeBomb(bombColor)
			this.props.makePointUp()
		} else {
			this.props.removeBomb(bombColor)
			this.props.makePointDown()
		}

	}

	render() {
		let styles = {
			position: 'relative',
			top: '50vh',
			opacity: 1
		}
		if(this.state.hide) {
			styles.opacity = 0
		}
		return (
			<div style={styles}>
				<div ref={ref=> this.bin = ref} id={this.props.binId} style={{...bin, background: this.props.color}}>
					<h2>Bin</h2>
					<span>{this.props.color}</span>
				</div>
			</div>
		)
	}
}

export default Bin;