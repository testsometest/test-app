local server -> http://ed90663c.ngrok.io/

api
    signup                  ->          /api/signup
    login                   ->          /api/auth
    change password         ->          /api/users/change-password
    check password          ->          /api/users/check-password
    forgot password         ->          /api/forget-password
    set new password        ->          /api/forget-password/set-new-password
    confirmation by email   ->          /api/confirmation

inbox service for testing confirmation by email ->  mailtrap.io

run
    npm install
    npm run build
    npm run server
    